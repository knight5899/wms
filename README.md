# 梦树仓储管理系统

### 介绍

仓库管理系统是通过入库业务、出库业务、仓库调拨、库存调拨和虚仓管理等功能，综合批次管理、物料对应、库存盘点、质检管理、虚仓管理和即时库存管理等功能综合运用的管理系统，有效控制并跟踪仓库业务的物流和成本管理全过程，实现完善的企业仓储信息管理。该系统可以独立执行库存操作，与其他系统的单据和凭证等结合使用，可提供更为完整全面的企业业务流程和财务管理信息。
### 联系方式
交流QQ群850834867

本公司承接各类系统定制开发，商务合作，产品定制，PDA及打印定制开发可以联系电话027-59763552

### 主要功能


1、基础资料

2、入库管理

3、库内管理

4、出库管理

5、库存管理


### 在线demo

账号密码：admin/111111，地址：http://www.thedreamtree.cn/tdt-vip-main

### 软件架构

基于SpringBoot+mysql+easyweb 3.1.5

目前版本为1.0后续增加功能和补充

### 使用方法

1、下载项目压缩包，解压

2、下载idea后open选择解压后的文件夹

3、等待idea编译

4、找到文件夹内的sql，放入mysql运行创建数据库，项目默认数据库名称为tdt_wms，用户名密码root/root

5、运行TdtApplication文件，浏览器访问http://127.0.0.1即可

### 项目预览


![输入图片说明](https://images.gitee.com/uploads/images/2020/0304/152126_af6bbb9f_6516777.png "QQ截图20200304152007.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0304/152159_9b7b2b70_6516777.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0304/152219_33d74e0b_6516777.png "QQ截图20200304152035.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0304/152240_1b03e905_6516777.png "QQ截图20200304152048.png")
